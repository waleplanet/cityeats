import 'dart:convert';
import 'dart:math';
import 'package:http/http.dart' as http;
import 'package:city_eats/restaurant.dart';

//Future<List<Restaurant>> loadRestaurants (int offset,int  limit) async{
//  final response = await http.get('https://jsonplaceholder.typicode.com/posts/1');
//
//  List<Restaurant> list;
//  if (response.statusCode == 200) {
//    // If server returns an OK response, parse the JSON.
//    return(jsonDecode(response.body) as List).map((value) => Restaurant.fromJson(value)).toList();
//    } else {
//    // If that response was not OK, throw an error.
//    throw Exception('Failed to load post');
//    }
//}

Future<List<Restaurant>> loadRestaurants(int offset, int limit) {
  var random = new Random();
  return Future.delayed(new Duration(seconds: 2 + random.nextInt(3)), () {
    return List.generate(limit, (index) {
      var id = offset + index;
      return new Restaurant(id: id, name: "Item $id");
    });
  });
}

