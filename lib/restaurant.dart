class Restaurant {
  String name;
  String photo;
  int id;

  Restaurant({this.name, this.photo, this.id});

  factory Restaurant.fromJson(Map<String, dynamic> json) {
    return Restaurant(name: json['name'], photo: json['photo'], id: json['id']);
  }
}
