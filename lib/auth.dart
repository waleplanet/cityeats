import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:rxdart/rxdart.dart';


class AuthService{

  final GoogleSignIn _googleSignIn = GoogleSignIn();
  final FirebaseAuth _auth = FirebaseAuth.instance;
  final Firestore db = Firestore.instance;

  Observable<FirebaseUser> user;

  AuthService() {
    user = Observable(_auth.onAuthStateChanged);
  }

  Future<FirebaseUser> googleSignIn() async {
      final GoogleSignInAccount googleUser = await _googleSignIn.signIn();
      final GoogleSignInAuthentication googleAuth =
      await googleUser.authentication;

      final AuthCredential credential = GoogleAuthProvider.getCredential(
        accessToken: googleAuth.accessToken,
        idToken: googleAuth.idToken,
      );
      print(credential);

      final FirebaseUser user = await _auth.signInWithCredential(credential);
      print("signed in " + user.displayName);
      return user;
  }

  void signInWithPhoneNumber(verificationId,smsCode) async{
    String _message;
    final AuthCredential credential = PhoneAuthProvider.getCredential(
      verificationId: verificationId,
      smsCode: smsCode,
    );
    try {
      final FirebaseUser user = await _auth.signInWithCredential(credential);
      final FirebaseUser currentUser = await _auth.currentUser();
      assert(user.uid == currentUser.uid);
        if (user != null) {
          _message = 'Successfully signed in, uid: ' + user.uid;
        } else {
          _message = 'Sign in failed';
        }
    }catch (e) {
      if (e.message.contains('The sms verification code used to create the phone auth credential is invalid')) {
        // ...
        _message = e.message;
      } else if (e.message.contains('The sms code has expired')) {
        // ...
        _message = e.message;
      }
      print(_message);
    }


  }


  void updateUserData(FirebaseUser user) async {


  }


  void signOut() {
    _auth.signOut();
  }
}

final AuthService authService = AuthService();
