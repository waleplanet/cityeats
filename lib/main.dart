
import 'package:city_eats/screens/splash.dart';
import 'package:flutter/material.dart';
import 'package:city_eats/screens/login.dart';

import 'app_route.dart';
import 'auth.dart';


void main() => runApp(MyApp());

class MyApp extends StatelessWidget {

  Widget _handleCurrentSession() {
    return new StreamBuilder(
        stream: authService.user,
        builder: (BuildContext context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return new SplashPage();
          }
          else {
            if (snapshot.hasData) {
              print(snapshot.data);
              return new AppRoute(store: authService.db , uuid: snapshot.data.uid);
            }
            return new LoginPage();
          }
        });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
          title: 'City Eats',
          theme: ThemeData(
              pageTransitionsTheme: PageTransitionsTheme(builders: {
              TargetPlatform.iOS: CupertinoPageTransitionsBuilder(),
              TargetPlatform.android: OpenUpwardsPageTransitionsBuilder(),
              }),
            primarySwatch: Colors.red,
            textTheme: TextTheme(
              display4: TextStyle(
                fontFamily: 'Corben',
                fontWeight: FontWeight.w700,
                fontSize: 24,
                color: Colors.black,
              ),
            ),
          ),
          home: _handleCurrentSession(),
//          routes: {
//            '/login': (context) => LoginPage(),
//            '/home': (context) => RestaurantsPage(firestore: null , uuid: null),
//          },
        );
  }
}
