
import 'package:city_eats/screens/filter_settings_menu.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:city_eats/screens/restaurants.dart';

class AppArguments {
  final Firestore store;
  final String uuid;
  AppArguments(this.store, this.uuid);
}

class AppRoute  extends StatelessWidget{

  final Firestore store;
  final String uuid;

  const AppRoute({
    Key key,
    @required this.store,
    @required this.uuid,
  }) : super(key: key);


  @override
  Widget build(BuildContext context) {

    return Navigator(
      initialRoute: '/',
      onGenerateRoute: (RouteSettings settings){
        WidgetBuilder builder;
        switch   (settings.name){
          case '/':
            builder = (BuildContext _)=>RestaurantsPage(firestore:store,uuid:uuid);
            break;
          case '/filter':
            builder = (BuildContext _)=>FilterSettingMenu();
            break;
          default:
            throw Exception('invalid route: ${settings.name}');
        }
        return MaterialPageRoute(builder: builder,settings: settings);
      },
    );
  }

}