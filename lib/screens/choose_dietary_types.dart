import 'package:flutter/material.dart';

import 'package:city_eats/components/app_bar.dart';

class DietRestrictionPage extends StatefulWidget {
  DietRestrictionPage({Key key}) : super(key: key);

  @override
  _DietRestrictionPageState createState() => _DietRestrictionPageState();
}

class _DietRestrictionPageState extends State<DietRestrictionPage> {
//  static bool international,european,mediterranean,middle_east,italian,japanese,chinese,mexican,american,indian = false;
//  final List<bool> states = [international,european,mediterranean,middle_east,italian,japanese,chinese,mexican,american,indian];
  static List<String> statesTitles = ['Vegetarian Friendly','Vegan options','Glutten free options'];
  static List<bool> states = new List<bool>.generate(statesTitles.length, (int index) => false); // [0, 1, 4]


  @override
  Widget build(BuildContext context) {
//    double maxWidth = MediaQuery.of(context).size.width * 0.7;

    return Scaffold(
        appBar: CustomAppBar(),
        body: Container(
            child: Column(
          children: <Widget>[
            Text(
              'Select dietary restrictions',
              style: Theme.of(context).textTheme.headline,
            ),
            Expanded(
              child: ListView.builder(
                  itemExtent: 80.0,
                  itemCount: states.length,
                  itemBuilder: (context, index) => buildCard(index)),
            )
          ],
        )));
  }

  Widget buildCard(index) {
    return Card(
      child: ListTile(
        title: Text(statesTitles[index]),
        trailing: Checkbox(
            value:states[index],
            onChanged: (value) => setState(() {
              states[index] = value;
              print(states);
            })),
      ),
    );
  }

}
