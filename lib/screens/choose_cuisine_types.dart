import 'package:flutter/material.dart';

import 'package:city_eats/components/app_bar.dart';

class CuisineTypePage extends StatefulWidget {
  CuisineTypePage({Key key}) : super(key: key);

  @override
  _CuisineTypePageState createState() => _CuisineTypePageState();
}

class _CuisineTypePageState extends State<CuisineTypePage> {
//  static bool international,european,mediterranean,middle_east,italian,japanese,chinese,mexican,american,indian = false;
//  final List<bool> states = [international,european,mediterranean,middle_east,italian,japanese,chinese,mexican,american,indian];
  static List<String> statesTitles = ['International','European','Mediterranean','Middle East','Italian','Japanese','Chinese','Mexican','American','Indian'];
  static List<bool> states = new List<bool>.generate(statesTitles.length, (int index) => false); // [0, 1, 4]


  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: CustomAppBar(),
        body: Container(
            child: Column(
          children: <Widget>[
            Text(
              'Choose Cuisine types',
              style: Theme.of(context).textTheme.headline,
            ),
            Expanded(
              child: ListView.builder(
                  itemExtent: 80.0,
                  itemCount: states.length,
                  itemBuilder: (context, index) => buildCard(index)),
            )
          ],
        )));
  }

  Widget buildCard(index) {
    return Card(
      child: ListTile(
        title: Text(statesTitles[index]),
        trailing: Checkbox(
            value:states[index],
            onChanged: (value) => setState(() {
              states[index] = value;
              print(states);
            })),
      ),
    );
  }

}
