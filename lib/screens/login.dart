import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import '../auth.dart';

final FirebaseAuth _auth = FirebaseAuth.instance;

class LoginPage extends StatefulWidget {

  @override
  _LoginPageState createState() => _LoginPageState();
}
class _LoginPageState extends State<LoginPage>{

  String _message;
  String _smsCode;
  String _verificationId;
  AuthCredential _authCredential;

  final TextEditingController _phoneNumberController = TextEditingController();



  @override
  Widget build(BuildContext context) {

    return Material(
        child: Center(
            child: Padding(
      padding: const EdgeInsets.symmetric(horizontal: 64),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
            child:  Text('Enter your phone number',style: Theme.of(context).textTheme.display4,textAlign: TextAlign.center),
            alignment: Alignment.center,
          ),
          SizedBox(
            height: 20,
          ),
          TextFormField(
            controller: _phoneNumberController,
            decoration: InputDecoration(
                prefix:
                    Text('+234'),
                hintText: 'phone number',
                fillColor: Colors.black54),
            validator: (String value) {
              if (value.isEmpty) {
                return '+234';
              }
              return null;
            },
          ),
          SizedBox(height: 6),
          FlatButton(
            color: Colors.red,
            onPressed: () {
              _verifyPhoneNumber();
            },
            child: Text("SIGN IN"),
          ),
          new RaisedButton(
            child: Text('Sign in with google'),
            onPressed: () {
              authService.googleSignIn();
            },
          )
        ],
      ),
    )));
  }


  void _verifyPhoneNumber() async{

    final PhoneVerificationCompleted verificationCompleted = (AuthCredential phoneAuthCredential){
      _auth.signInWithCredential(phoneAuthCredential);
      setState((){
        _authCredential = phoneAuthCredential;
        _message = 'Received phone auth credential: $phoneAuthCredential';
      });
    };

    final PhoneVerificationFailed verificationFailed = (AuthException exception){
      setState(() {
        _message = 'Phone number verification failed.Code: ${exception.code}. Message: ${exception.message}';
      });
      print( _message);

    };

    final PhoneCodeSent codeSent =
        (String verificationId, [int forceResendingToken]) async {
          this._verificationId = verificationId;
          smsCodeDialog(context);
    };

    final PhoneCodeAutoRetrievalTimeout codeAutoRetrievalTimeout =
        (String verificationId) {
      _verificationId = verificationId;
    };

    await _auth.verifyPhoneNumber(
        phoneNumber: _phoneNumberController.text,
        timeout: const Duration(seconds: 5),
        verificationCompleted: verificationCompleted,
        verificationFailed: verificationFailed,
        codeSent: codeSent,
        codeAutoRetrievalTimeout: codeAutoRetrievalTimeout);
  }

  Future<bool> smsCodeDialog(BuildContext context) {
    return showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return new AlertDialog(
            title: Text('Enter sms Code'),
            content: TextField(
              onChanged: (value) {
                setState(() {
                  _smsCode = value;
                });
              },
            ),
            contentPadding: EdgeInsets.all(10.0),
            actions: <Widget>[
              new FlatButton(
                child: Text('Done'),
                onPressed: () {
                  FirebaseAuth.instance.currentUser().then((user) {
                    if (user != null) {Navigator.of(context).pop();
                      Navigator.of(context).pushReplacementNamed('/home');
                    } else {
                      Navigator.of(context).pop();
                      authService.signInWithPhoneNumber(_verificationId,_smsCode);
                    }
                  });
                },
              ),

            ],
          );
        });
  }




}
