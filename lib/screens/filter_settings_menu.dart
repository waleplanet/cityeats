import 'package:flutter/material.dart';

import 'package:city_eats/components/app_bar.dart';

class FilterSettingMenu extends StatefulWidget {
  FilterSettingMenu({Key key}) : super(key: key);

  @override
  _FilterSettingsMenuState createState() => _FilterSettingsMenuState();
}

class _FilterSettingsMenuState extends State<FilterSettingMenu> {
  bool openNow = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(),
      body: Container(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Card(
              child: ListTile(
                  title: Text("Open now",
                      style: TextStyle(fontWeight: FontWeight.w500)),
                  trailing: Switch(
                      value: openNow,
                      onChanged: (value) => setState(() {
                            openNow = value;
                          }),
                      activeColor: Colors.red,
                      activeTrackColor: Colors.redAccent)),
            ),
            Divider(),
            Card(
              child: ListTile(
                  title: Text("Establishment",
                      style: TextStyle(fontWeight: FontWeight.w500)),
                  leading: Icon(Icons.business),
                  subtitle: Text('not chosen'),
                  onTap: () => Navigator
                      .pushNamed(context,'filter/establishment_types')),
            ),
            Divider(),
            Card(
                child: ListTile(
                    title: Text("Cuisine",
                        style: TextStyle(fontWeight: FontWeight.w500)),
                    leading: Icon(Icons.restaurant_menu),
                    subtitle: Text('not chosen'),
                    onTap: () => Navigator.pushNamed(context,'filter/cuisine_types'))),
            Card(
                child: ListTile(
                    title: Text("Dietary Restrictions",
                        style: TextStyle(fontWeight: FontWeight.w500)),
                    leading: Icon(Icons.room_service),
                    subtitle: Text('not chosen'),
                    trailing: IconButton(
                        icon: Icon(Icons.keyboard_arrow_right),
                        onPressed: () => Navigator.pushNamed(context,'filter/dietary_types'))))
          ],
        ),

//            ],
      ),
//        ),
//      ),
    );
  }
}
