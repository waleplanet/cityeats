import 'dart:async';
//import 'dart:math';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:city_eats/components/app_bar.dart';
//import '../funcs.dart';
import '../restaurant.dart';

class RestaurantsPage extends StatefulWidget {
  RestaurantsPage({Key key,this.firestore,this.uuid})
      : super(key: key);

  final String uuid;
  final Firestore firestore;

  @override
  _RestaurantsPageState createState() => _RestaurantsPageState();
}

class _RestaurantsPageState extends State<RestaurantsPage> {
  var total = 105;
  var pageSize = 20;

  var completers = new List<Completer<Restaurant>>();

//  Widget _listItem(int itemIndex){
//    if(itemIndex >= completers.length){
//      int toLoad = min(total - itemIndex, pageSize);
//      completers.addAll(List.generate(toLoad, (index){
//        return new Completer();
//      }));
//      loadRestaurants(itemIndex,toLoad).then((items) => items.asMap().forEach((index,item) => completers[itemIndex + index].complete(item)))
//          .catchError((error)=> completers.sublist(itemIndex,itemIndex+toLoad).forEach((completer) => completer.completeError(error)));
//
//    }
//
//    var future =  completers[itemIndex].future;
//    return new FutureBuilder(
//        future: future,
//        builder: (context, snapshot)
//    {
//      switch (snapshot.connectionState) {
//        case ConnectionState.waiting:
//          return new Container(
////            padding: const EdgeInsect.all(8.0),
//            child: new Placeholder(fallbackHeight: 100.0),
//          );
//        case ConnectionState.done:
//          if(snapshot.hasData){
//            return _generateItem(snapshot.data);
//          }else if(snapshot.hasError){
//            return new Text(
//              '${snapshot.error}',
//              style: TextStyle(color: Colors.red),
//            );
//          }
//          return new Text('');
//        default:
//          return new Text('');
//      }
//    });
//  }
  Widget _listItem(BuildContext context, DocumentSnapshot document) {
    return ListTile(
        onTap: () {
          print("Restaurant tapped");
        },
        title: Row(
          children: <Widget>[
            Expanded(
              child: Text(
                document['name'],
                style: Theme.of(context).textTheme.headline,
              ),
            ),
            Container(
              decoration: const BoxDecoration(
                color: Color(0xffddddff)
              ),
              padding: const EdgeInsets.all(8.0),
              child:  new Image.network(
                'http://via.placeholder.com/200x100?text=Item${document['id']}',
                width: 200.0,
                height: 100.0,
              ),
            )
          ],
      ),
    );
  }

  Widget build(BuildContext context) {
//    double maxWidth = MediaQuery.of(context).size.width * 0.7;
    return Scaffold(
      appBar: CustomAppBar(),
      body: Center(
        child: StreamBuilder(
            stream: widget.firestore.collection('restaurants').snapshots(),
            builder: (context, snapshot) {
              if (!snapshot.hasData) return const Text('Loading...');
              return ListView.builder(
                  itemExtent: 80.0,
                  itemCount: snapshot.data.documents.length,
                  itemBuilder: (context, index) =>
                      _listItem(context, snapshot.data.documents[index]));
            }),
      ),
      floatingActionButton: FloatingActionButton(
        tooltip: 'Increment',
        child: Icon(Icons.add),
        onPressed: () => {},
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
