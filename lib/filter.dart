import 'package:city_eats/screens/choose_cuisine_types.dart';
import 'package:city_eats/screens/choose_dietary_types.dart';
import 'package:city_eats/screens/choose_establishment_types.dart';
import 'package:city_eats/screens/filter_settings_menu.dart';
import 'package:flutter/material.dart';

class FilterSetting  extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return Navigator(
      initialRoute: '/',
      onGenerateRoute: (RouteSettings settings){
        WidgetBuilder builder;
        switch   (settings.name){
          case '/filter':
            builder = (BuildContext _)=>FilterSettingMenu();
            break;
          case 'filter/cuisine_types':
            builder = (BuildContext _)=>CuisineTypePage();
            break;
          case 'filter/dietary_types':
            builder = (BuildContext _)=>DietRestrictionPage();
            break;
          case 'filter/establishment_types':
            builder = (BuildContext _)=>EstablishmentTypePage();
            break;
          default:
            throw Exception('invalid route: ${settings.name}');
        }
        return MaterialPageRoute(builder: builder,settings: settings);
      },
    );
  }

}